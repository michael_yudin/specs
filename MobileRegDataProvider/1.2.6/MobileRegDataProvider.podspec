Pod::Spec.new do |s|
	s.name         = 'MobileRegDataProvider'
	s.version      = '1.2.6'
	s.license      =  {:type => 'MIT', :file => "LICENSE" }
	s.homepage     = 'https://bitbucket.org/michael_yudin/mobileregdataprovider/overview'
	s.authors      =  {'Michael Yudin' => 'michael.s.yudin@gmail.com'}
	s.summary      = 'Data Provider'

# Source Info
	s.platform     =  :ios, '7.1'
	s.source       =  {:git => 'https://michael_yudin@bitbucket.org/michael_yudin/mobileregdataprovider.git', :tag => '1.2.6'}
	s.source_files =  'MobileRegDataProvider/*.{h,m}','MobileRegDataProvider/Client/*.{h,m}','MobileRegDataProvider/Helpers/*.{h,m}','MobileRegDataProvider/Models/*.{h,m}'
#  s.private_header_files = 'Pods/*.{h,m}'
#  s.source_files = '**/*.{h,m}'
#'MobileRegDataProvider/**/*.{h,m}'
    s.prefix_header_file = 'MobileRegDataProvider-Prefix.pch'
	s.requires_arc = true
	s.resource = "MobileRegDataProvider/mobilereg.xcdatamodeld"
  
# Pod Dependencies
	s.dependency 'Mantle'
	s.dependency 'PromiseKit'
	s.dependency 'AFNetworking', '~> 2.5'
	s.dependency 'MDTGRDataSource'
	s.dependency 'MDOvercoat'
	s.dependency 'MDOvercoat/PromiseKit'
	s.dependency 'SVProgressHUD'

	
# Subspecs
# 	s.subspec "Overcoat" do |ss|
# 	  ss.dependency 'Overcoat'
# 	  ss.public_header_files = 'Overcoat/*.h'
# #	  ss.source_files = 'Pods/Overcoat/Overcoat'
# 	end
# 	
# # 	s.subspec 'PromiseKit' do |ss|
# # 		ss.dependency 'Overcoat/Core'
# # 		ss.dependency 'Overcoat/NSURLSession'
# # 		ss.dependency 'PromiseKit', '~>1.2'
# # 		
# # 		ss.public_header_files = 'PromiseKit+Overcoat/*.h'
# # 		ss.source_files = 'PromiseKit+Overcoat'
# # 	end
# 
# 	s.subspec "TGRDataSource" do |sp|
# 	  sp.source_files = 'Pods/TGRDataSource'
# 	end

end